FROM openjdk:11.0.14
ADD target/MaBanque-0.0.1-SNAPSHOT.jar MaBanque-0.0.1-SNAPSHOT.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "MaBanque-0.0.1-SNAPSHOT.jar"]
